# Atomizer

[![CI](https://github.com/devnw/atomizer/workflows/CI/badge.svg)](https://github.com/devnw/atomizer/actions)
[![Go Report Card](https://goreportcard.com/badge/github.com/devnw/atomizer)](https://goreportcard.com/report/github.com/devnw/atomizer)
[![codecov](https://codecov.io/gh/devnw/atomizer/branch/master/graph/badge.svg)](https://codecov.io/gh/devnw/atomizer)
[![GoDoc](https://godoc.org/github.com/devnw/atomizer?status.svg)](https://pkg.go.dev/github.com/devnw/atomizer)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](http://makeapullrequest.com)

Atomizer is a library that facilitates the execution of distributed atomic
actions across a cluster of nodes.

