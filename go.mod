module github.com/devnw/atomizer

go 1.14

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/devnw/alog v1.0.1
	github.com/devnw/validator v1.0.2
	github.com/google/uuid v1.1.1
	github.com/pkg/errors v0.9.1
	golang.org/x/text v0.3.2 // indirect
)
